# clean project
./gradlew clean

# build project
./gradlew build

# run Heuristics.java
./gradlew runHeuristic

# run Classification.java
./gradlew runClassification

# run AttributeSelectionTest.java
./gradlew runAttributeSelectionTest

# run CrossValidationClassification.java
./gradlew runCrossValidationClassification

# run Spearman.java
./gradlew runSpearman

