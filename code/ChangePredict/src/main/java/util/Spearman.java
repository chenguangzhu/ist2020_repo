package util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import com.csvreader.CsvReader;

public class Spearman {
    final static SpearmansCorrelation SC = new SpearmansCorrelation();
    public String datapath;
    public Map<String, ArrayList<Double>> All_X = new HashMap<String, ArrayList<Double>>();
    public ArrayList<Double> Y = new ArrayList<Double>();
    public Map<String, Double> Spearman_X = new HashMap<String, Double>();
   
    public Spearman(String path) {
    	this.datapath = path;
	}

	public void loadData() throws IOException {
    	 CsvReader reader = new CsvReader(datapath);
		 reader.readHeaders();
		 String [] heads = reader.getHeaders();
		int b=0;
		 while(reader.readRecord()) {
			 String id =reader.get(0);
			 b = b+1;
			 for(int a =1; a< heads.length-1; a++) {
				// System.out.println(b);
				String colName = heads[a];
				//System.out.println(colName);
				double data = Double.valueOf(reader.get(colName));
				
				if(!All_X.containsKey(colName)) {
					ArrayList<Double> data_col = new ArrayList<Double>();
					data_col.add(data);
					All_X.put(colName, data_col);
				}else {
					ArrayList<Double> data_col = new ArrayList<Double>();
					data_col = All_X.get(colName);
					data_col.add(data);
					All_X.put(colName, data_col);
				}
			 }
			 
			  double label = Double.valueOf(reader.get("label"));
			  Y.add(label);
		 }
    }
    
    public void analysis() {
    	for(Map.Entry<String, ArrayList<Double>> entry: All_X.entrySet()) {
    		String colName = entry.getKey();
    		ArrayList<Double> data = entry.getValue();
    		Spearman_X.put(colName, this.getCorrelation(data, Y));
    	}
    }
    
    public double getCorrelation(List<Double> X, List<Double> Y) {
       double[] xArray = toDoubleArray(X);
       double[] yArray = toDoubleArray(Y);
       double corr = SC.correlation(xArray, yArray);
       return corr;
    }
    
    public  double getPvalue(final double corr, final int n) {
       return getPvalue(corr, (double) n);
    }
    
    public  double getPvalue(double corr, double n) {
       double t = Math.abs(corr * Math.sqrt( (n-2.0) / (1.0 - (corr * corr)) ));
       System.out.println("     t = "+ t);	
       TDistribution tdist = new TDistribution(n-2);
       double pvalue = 2.0 * (1.0 - tdist.cumulativeProbability(t));	// p-value worked. 		
       return pvalue;
    }

    public  double[] toDoubleArray(List<Double> list) {
       double[] arr = new double[list.size()];
       for (int i=0; i < list.size(); i++) {
          arr[i] = list.get(i);
       }
       return arr;
    }
    
    private static Map<String, Double> sortByValue(Map<String, Double> unsortMap) {

        List<Map.Entry<String, Double>> list =
                new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> o1,
                               Map.Entry<String, Double> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Map.Entry<String, Double> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
    
    public  static void main(String [] args) throws Exception {
        // Missing file:
    	//String path="C:\\Users\\t-sowan\\Tunning\\arff-type-file-code-churn-word2vec\\all changes (exo-2018-5-24).csv";
    	String path="../../data/android/arff/all.csv";

    	Spearman spearman = new Spearman(path);
    	spearman.loadData();
    	spearman.analysis();
    	Map<String, Double> Spearman_X1 = sortByValue(spearman.Spearman_X);
    	
    	for(Map.Entry<String, Double> entry: Spearman_X1.entrySet()) {
    		if(entry.getValue()>=0.1 || entry.getValue()<= -0.1)
    		System.out.println(entry.getKey()+","+entry.getValue());
    	}
//    	List<Double> x = new ArrayList<Double>();
//    	List<Double> y = new ArrayList<Double>();
//    	x.add(1.0); x.add(2.0); x.add(3.0); x.add(4.0); x.add(5.0);
//    	y.add(5.0); y.add(6.0); y.add(7.0); y.add(8.0); y.add(7.0);
//    	System.out.println(spearman.getCorrelation(x,y));
    }
}