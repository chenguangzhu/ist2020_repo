package heuristic.labelling;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.csvreader.CsvReader;

public class Heuristics {
	public String commentPath;
	public String reviewPath;
	public String changePath;
	public Map<String, String> reviews = new HashMap<String, String>();
	public Map<String, String> comments = new HashMap<String, String>();
	public ArrayList<String> bug = new ArrayList<String>();
	public ArrayList<String> feature = new ArrayList<String>();
	public ArrayList<String> test = new ArrayList<String>();
	public ArrayList<String> resource = new ArrayList<String>();
	public ArrayList<String> merge = new ArrayList<String>();
	public ArrayList<String> deprecat = new ArrayList<String>();
	public ArrayList<String> refactoring = new ArrayList<String>();
	public ArrayList<String> lefts = new ArrayList<String>();
	public ArrayList<String> all = new ArrayList<String>();
	
	public Map<String, ArrayList<String>> changedFiles = new HashMap<String, ArrayList<String>>();
	
	public Heuristics(String commentPath, String reviewPath, String changePath) {
		super();
		this.commentPath = commentPath;
		this.reviewPath = reviewPath;
		this.changePath = changePath;
	}

	public void loadData() throws IOException {
		
		CsvReader  reader = new CsvReader(reviewPath);
		reader.readHeaders();
		while (reader.readRecord()) {
			String id = reader.get(0).trim();
			id = id.trim();
			//status,patchSets
			String status = reader.get("status").trim();
			int patchset = Integer.valueOf(reader.get("patchSets").trim());
			if(status.equals("M") && patchset>1)
				reviews.put(id, "1");
			if(status.equals("M") && patchset==1)
				reviews.put(id, "0");
		}
		 int redo =0, regualr =0;
		 for(String str: reviews.values()) {
			 if(str.equals("1"))
				 redo++;
			 else
				 regualr++;
		 }
		System.out.println("reviews size: "+reviews.size());
		System.out.println("	redo "+redo);
		reader = new CsvReader(commentPath);
		reader.readHeaders();
		while (reader.readRecord()) {
			String id = reader.get(0).trim();
			if(reviews.containsKey(id)) {
				String comment = reader.get(1).trim();
				comments.put(id, comment);
			}
		}
		
		
		
		reader = new CsvReader(changePath);
		reader.readHeaders();
		while (reader.readRecord()) {
			String id = reader.get(0).trim();
			id = id.trim();
			
			String file = reader.get(2).trim();
			if(changedFiles.containsKey(id)) {
				ArrayList<String> files = changedFiles.get(id);
				files.add(file);
				changedFiles.put(id, files);
			}else {
				ArrayList<String> files = new ArrayList<String>();
				files.add(file);
				changedFiles.put(id, files);
			}
		}
	}
	
	public void analysis() {
		for(Map.Entry<String, String> entry: comments.entrySet()) {
			String id = entry.getKey();
			
			String comment = entry.getValue();
			
			if(isbug(comment))
				bug.add(id);
			if(isfeature(comment))
				feature.add(id);
			if(istest(id, comment))
				test.add(id);
			if(isdeprecat(comment))
				deprecat.add(id);
			if(ismerge(comment))
				merge.add(id);
			if(isresource(id, comment))
				resource.add(id);
			if(isrefactoring(comment))
				refactoring.add(id);
		
		}
		
		for(String id: reviews.keySet()){
			if(!bug.contains(id) && !feature.contains(id)&& !test.contains(id) &&
			   !deprecat.contains(id) && !merge.contains(id) &&!resource.contains(id) &&!refactoring.contains(id)) {
				lefts.add(id);
			}
		}
		
		for(String id: lefts) {
			String comment = comments.get(id);
			if(isfeature_step2(comment))
				feature.add(id);
		}
		
		for(String id: feature){
			if(lefts.contains(id)) {
				lefts.remove(id);
			}
		}
	}
	
	public boolean isbug(String comment) {
		boolean isbug = false;
		
		Pattern pattern = null;
		pattern = Pattern.compile("fix|bug", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
			while (matcher.find()) {
				isbug = true;
			}
		return isbug;
	}
	
	public boolean isfeature(String comment) {
		boolean isfeature = false;
		
		Pattern pattern = Pattern.compile("(update\\S+\\b).*(feature\\S+\\b)", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
				isfeature = true;
		}
		
		pattern = Pattern.compile("(add\\S+\\b).*(feature\\S+\\b)", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(comment);
		while (matcher.find()) {
				isfeature = true;
		}
		pattern = Pattern.compile("(new\\S+\\b).*(feature\\S+\\b)", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(comment);
		while (matcher.find()) {
				isfeature = true;
		}
		pattern = Pattern.compile("(create\\S+\\b).*(new\\S+\\b)", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(comment);
		while (matcher.find()) {
				isfeature = true;
		}
		pattern = Pattern.compile("(add\\S+\\b).*(new\\S+\\b)", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(comment);
		while (matcher.find()) {
				isfeature = true;
		}
		pattern = Pattern.compile("implement", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(comment);
		while (matcher.find()) {
				isfeature = true;
		}
		
		return isfeature;
	}
	
	public boolean isfeature_step2(String comment) {
		boolean isfeature = false;
		
		Pattern pattern = Pattern.compile("enable|add|update|improve|support|new", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			isfeature = true;
		}
		
		return isfeature;
	}
	
	public boolean istest(String id, String comment) {
		boolean istest = false;
		boolean changeTest = true; 
		
		if(this.changedFiles.containsKey(id.trim())) {
			ArrayList<String> files = this.changedFiles.get(id.trim());
			for(String file: files) {
				if(file.trim().endsWith("c")||file.trim().endsWith("cpp")||file.trim().endsWith("java")
						 ||file.trim().endsWith("sql")||file.trim().endsWith("js")||file.trim().endsWith("h")||
						 file.trim().endsWith("sources")||file.trim().endsWith("dll")||file.trim().endsWith("cc")||file.trim().endsWith("py")) {
					if(file.toLowerCase().contains("test"))
						changeTest = false;
				}
			}
		}
		
		Pattern pattern = Pattern.compile("test|testing", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			istest = true;
		}
		
		return istest; //&& changeTest;
	}
	
	public boolean isdeprecat(String comment) {
		boolean isdeprecat = false;
		boolean morekeys = false;
		boolean bug_fix_match = false;
		Pattern pattern = Pattern.compile("deprecat", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			isdeprecat = true;
		}
		
		//|delete |deleted|remove |removed
		 pattern = Pattern.compile("delete |deleted|remove |removed|disable", Pattern.CASE_INSENSITIVE);
		 matcher = pattern.matcher(comment);
			while (matcher.find()) {
				morekeys = true;
			}
			
		 pattern = Pattern.compile("fix|bug", Pattern.CASE_INSENSITIVE);
		 matcher = pattern.matcher(comment);
			while (matcher.find()) {
				bug_fix_match = true;
			}		
			
		return isdeprecat || (morekeys); //&& !bug_fix_match);
	}
	
	public boolean ismerge(String comment) {
		boolean ismerge = false;
		boolean hasfail= false;
		Pattern pattern = Pattern.compile("merge |merging|merged |integrate", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			ismerge = true;
		}
		
		pattern = Pattern.compile("fail|fix", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(comment);
		while (matcher.find()) {
			hasfail = true;
		}
		
		return ismerge && !hasfail;
	}
	
	public boolean isrefactoring(String comment) {
		boolean isrefactoring = false;
		Pattern pattern = Pattern.compile("refactor|refactoring|refact|style", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			isrefactoring = true;
		}
		return isrefactoring;
	}
	
	public boolean isresource(String id, String comment) {
		boolean isresource = true;
		boolean keywordsMatching = false; 
		if(this.changedFiles.containsKey(id.trim())) {
			ArrayList<String> files = this.changedFiles.get(id.trim());
			for(String file: files) {
				if(file.trim().endsWith("c")||file.trim().endsWith("cpp")||file.trim().endsWith("java")
						 ||file.trim().endsWith("sql")||file.trim().endsWith("js")||file.trim().endsWith("h")||
						 file.trim().endsWith("sources")||file.trim().endsWith("dll")||file.trim().endsWith("cc")||file.trim().endsWith("py")) {
					isresource = false;
				}
			}
		}
		
		Pattern pattern = Pattern.compile("conf|license|legal", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			keywordsMatching = true;
		}
		
		return isresource || keywordsMatching;
	}
	
	public void write2file(String filepath, ArrayList<String> data) throws Exception, Exception {
		 PrintWriter writer = new PrintWriter(filepath, "UTF-8");
		 writer.println("Id");
		 int redo =0, regualr =0;
		 for(String str: data) {
			 if(this.reviews.get(str).equals("1"))
				 redo++;
			 else
				 regualr++;
			writer.println(str);
			writer.flush();
		}
		 writer.close();
		 System.out.println("	LRE  "+redo);
		 System.out.println("	regualr  "+regualr);
	}
	
	public ArrayList<String> get(String type){
		if(type.equals("bug"))
			return this.bug;
		if(type.equals("feature"))
			return this.feature;	
		if(type.equals("test"))
			return this.test;	
		if(type.equals("merge"))
			return this.merge;	
		if(type.equals("deprecat"))
			return this.deprecat;	
		if(type.equals("refactoring"))
			return this.refactoring;		
		if(type.equals("resource"))
			return this.resource;	
		if(type.equals("others"))
			return this.lefts;
		return null;
	}
	

	
	public static void main(String args []) throws Exception {
		String dir ="../../data/";
		String project = "android";
		String commentPath = dir+project+"/"+project+"-messages.csv";
		String reviewPath = dir+project+"/"+project+"-reviews.csv";
		String changefilePath = dir+project+"/"+project+"-files.csv";
		String output = dir+project+"/category/";

		Heuristics ruc = new Heuristics(commentPath, reviewPath, changefilePath);
		ruc.loadData();
		//ruc.write2Data(outputBothComments);
		ArrayList<String> types = new ArrayList<String>();
		types.add("bug");
		types.add("resource");
		types.add("feature");
		types.add("test");
		types.add("refactoring");
		types.add("merge");
		types.add("deprecat");
		types.add("others");
		
		//RemoveUnnucessaryComments ruc = new RemoveUnnucessaryComments(commentPath);
	
		ruc.analysis();
		for(String str: types) {
		System.out.println(str+" "+ruc.get(str).size());
		ruc.write2file(output+str+".csv", ruc.get(str));
		}
	}
}