package classification;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
//import weka.classifiers.bayes.NaiveBayesSimple;
import weka.classifiers.functions.Logistic;
import weka.classifiers.trees.J48;
//import weka.classifiers.trees.ADTree;
import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.filters.Filter;
import weka.filters.supervised.instance.SpreadSubsample;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveWithValues;

public class Classification {
	public String TrainingPath;
	public String resultPath;
	public String MappingPath;
	public String raw_tokens;
	public String allChanges;
	public Map<String, String> changeRaws = new HashMap<String, String>();
	public Map<String, String> changeLabels = new HashMap<String, String>();
	/**
	 * used for F1
	 */
	public double TruthPositive = 0;
	public double TrueNegative = 0;
	public double FalseNegative = 0;
	public double FalsePositive = 0;
	public double TestingInstance = 0;
	
	public  Instances TrainingData;
	public  Instances TestingData;
	

	public Map<String, String> mappings = new HashMap<String, String>();
	
	private FileWriter writer;
	private FileReader fr;
	private BufferedReader br;
		
	
	public Classification(String trainingPath, String resultPath) {
		super();
		TrainingPath = trainingPath;
		this.resultPath = resultPath;
	}
	
	/**
	 * @desc loading training and testing data
	 * @throws Exception
	 */
	public void loadData() throws Exception {
		ArffLoader loader = new ArffLoader();
		loader.setSource(new File(this.TrainingPath));
		Instances trainingSet = loader.getDataSet();
		trainingSet.setClassIndex(trainingSet.numAttributes() - 1);
		
		trainingSet.randomize(new java.util.Random(0));
		
		int trainSize = (int) Math.round(trainingSet.numInstances() * 0.66);
		int testSize = trainingSet.numInstances() - trainSize;
		TrainingData = new Instances(trainingSet, 0, trainSize);
		TestingData = new Instances(trainingSet, trainSize, testSize);
	
		
	}

	/**
	 * @desc: undersample
	 * @param type
	 * @throws Exception 
	 */
	public  void undersampleForTraining() throws Exception{                     
			 TrainingData = sample(TrainingData);  
	}

	private  Instances sample(Instances data) throws Exception{
		SpreadSubsample sampler = new SpreadSubsample();
		String Fliteroptions="-M 0.85 -X 0.0 -S 1";
		sampler.setOptions(weka.core.Utils.splitOptions(Fliteroptions));
		sampler.setInputFormat(data);
		Instances newData = Filter.useFilter(data, sampler);
		return newData;
	}
	
	public  void ADTree() throws Exception{
		 String[] option = new String[4];
		 //-3(all), -2(weight), -1(z_pure), >=0 seed for random walk
		 option[0] = "-B";
		 option[1] = "20";
		 option[2] = "-E";
		 option[3] = "-3";
		 J48 tree = new J48();  

		 //tree.setOptions(option);   
		 
		 Classifier cls = tree;
		 cls.buildClassifier(TrainingData);
		 Evaluation eval = new Evaluation(TrainingData);
		 
		 eval.evaluateModel(cls, TestingData);
		 
		 TruthPositive = eval.numTruePositives(1);
		 TrueNegative = eval.numTrueNegatives(1);
		 FalseNegative = eval.numFalseNegatives(1);
		 FalsePositive = eval.numFalsePositives(1);
		 TestingInstance = eval.numInstances();
		 
		 System.out.println("======ADTree======");
		 System.out.println("prediction precision: "+eval.precision(1));
		 System.out.println("prediction recall: " +eval.recall(1));
		 System.out.println("prediction fMeasure: "+eval.fMeasure(1));
		 System.out.println("prediction AUC: "+eval.areaUnderROC(1));
		 //System.out.println(cls.toString());
	}
	
	public  void niveBayes() throws Exception{
		 NaiveBayes naive = new NaiveBayes();   
		 
		 Classifier cls = naive;
		 cls.buildClassifier(TrainingData);
		 Evaluation eval = new Evaluation(TrainingData);
		 eval.evaluateModel(cls, TestingData);
		 
		 TruthPositive = eval.numTruePositives(1);
		 TrueNegative = eval.numTrueNegatives(1);
		 FalseNegative = eval.numFalseNegatives(1);
		 FalsePositive = eval.numFalsePositives(1);
		 TestingInstance = eval.numInstances();
		 
		 System.out.println("======Naive Bayes======");
		 System.out.println("prediction precision: "+eval.precision(1));
		 System.out.println("prediction recall: " +eval.recall(1));
		 System.out.println("prediction fMeasure: "+eval.fMeasure(1));
		 System.out.println("prediction AUC: "+eval.areaUnderROC(1));
		 //System.out.println(cls.toString());
	}
	
	public  void randomForest() throws Exception{
		RandomForest base = new RandomForest();
		 
		 Classifier cls = base;
		 cls.buildClassifier(TrainingData);
		 Evaluation eval = new Evaluation(TrainingData);
		 eval.evaluateModel(cls, TestingData);
		 
		 TruthPositive = eval.numTruePositives(1);
		 TrueNegative = eval.numTrueNegatives(1);
		 FalseNegative = eval.numFalseNegatives(1);
		 FalsePositive = eval.numFalsePositives(1);
		 TestingInstance = eval.numInstances();
		 
		 System.out.println("======RandomForest======");
		 System.out.println("prediction precision: "+eval.precision(1));
		 System.out.println("prediction recall: " +eval.recall(1));
		 System.out.println("prediction fMeasure: "+eval.fMeasure(1));
		 System.out.println("prediction AUC: "+eval.areaUnderROC(1));
		 //System.out.println(cls.toString());
	}
	

	public  void logistic() throws Exception{
		 Logistic log = new Logistic();   
		 Classifier cls = log;
		 cls.buildClassifier(TrainingData);
		 Evaluation eval = new Evaluation(TrainingData);
		 
		 
		 
		 eval.evaluateModel(cls, TestingData);
		 
		 TruthPositive = eval.numTruePositives(1);
		 TrueNegative = eval.numTrueNegatives(1);
		 FalseNegative = eval.numFalseNegatives(1);
		 FalsePositive = eval.numFalsePositives(1);
		 TestingInstance = eval.numInstances();
		 
		 System.out.println("======Logistic======");
		 System.out.println("prediction precision: "+eval.precision(1));
		 System.out.println("prediction recall: " +eval.recall(1));
		 System.out.println("prediction fMeasure: "+eval.fMeasure(1));
		 System.out.println("prediction AUC: "+eval.areaUnderROC(1));
		 //System.out.println(cls.toString());
	}
	
	//write to file
	public void write2File() throws Exception{
		 writer = new FileWriter(this.resultPath, true);
		 PrintWriter out = new PrintWriter(writer);
			 out.println(this.TruthPositive+","+
					 	this.TrueNegative+","+this.FalsePositive+","+this.FalseNegative);
			 out.flush();
	}
		
	
	public static void main(String[]args) throws Exception{
			ArrayList<String> types = new ArrayList<String>();
			

			types.add("bug");
			types.add("resource");
			types.add("feature");
			types.add("test");
			types.add("refactoring");
			types.add("merge");
			types.add("deprecated");
			types.add("others");
			types.add("all");
			
			for(String str: types) {
			System.out.println("now is handling :"+str);

			String project = "android";
			
			String TrainingPath = "../../data/"+ project + "/arff/" + str + ".arff";
			String resultPath = "../../data/" + project + "/result/" + str + "-result.txt";

			Classification classifier = new Classification(TrainingPath,  resultPath);
			classifier.loadData();
			//if(str.equals("resource"))
			classifier.undersampleForTraining();
			classifier.niveBayes();
			classifier.logistic();
			classifier.ADTree();
			//dtree.write2File();
			
			}
	}
}
